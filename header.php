<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width = device-width, initial-scale=1">
    <title>
        <?php
        wp_title('|', true, 'right');
        bloginfo('name');
    ?>
    </title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>" />
    <?php wp_head(); ?>
    <<style>
        .home-hero-section_NBR {
            background: url(<?php header_image(); ?>) no-repeat #d3d2d0;
            background-size: 100%;
            background-position: 0 60%;
            padding-bottom: 215px
        }
    </style>

</head>

<body data-wz_page_group="home" sr-scroll-left="0" sr-scroll-top="2600">
    <div id="app">
        <div id="cart_alert" class="cart-alert-box_5T_ small_1Jg"></div>
        <div name="app-header"></div>
        <div class="header__placeholder_8pH"></div>
        <div class="header__backdrop_4pF"></div>
        <header id="site-header" class="site-header_1WD">
            <a class="logo_19w" data-test-id="logo" href="/">
            <?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            ?>
                <img src="<?php echo $image[0];?>" alt="">
            </a>
            
            <?php get_sidebar('menu'); ?>
            <div class="actions_gY2"><a class="tablet-only_2pL" href="/mattress">Shop Mattress</a>
                <div id="header_chat" class="chat_29l"><svg xmlns="http://www.w3.org/2000/svg" height="40px"
                        width="40px" viewBox="0 0 50 50" fill="#4C3043" class="chat__icon_2rD icon_3C6">
                        <g>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M10.8893 37.5H47.3684V2.63158H2.63158V44.3815L10.8893 37.5ZM0 50V0H50V40.1316H11.8421L0 50Z">
                            </path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M18.4211 15.7895L18.4211 23.6842H15.7895L15.7895 15.7895H18.4211Z"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M26.3158 15.7895L26.3158 23.6842H23.6842L23.6842 15.7895H26.3158Z"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M34.2105 15.7895L34.2105 23.6842H31.5789L31.5789 15.7895H34.2105Z"></path>
                        </g>
                    </svg></div>
                <div data-test-id="cart" class="cart_S6b">
                    <div></div>
                </div>
                <div data-test-id="nav_toggle_burger" class="nav-toggle_2jK"><svg xmlns="http://www.w3.org/2000/svg"
                        height="13px" width="18px" viewBox="0 0 18 13" fill="none" class="icon_3C6">
                        <g>
                            <line y1="0.5" x2="18" y2="0.5" stroke="#4C3043"></line>
                            <line y1="4.5" x2="18" y2="4.5" stroke="#4C3043"></line>
                            <line y1="8.5" x2="18" y2="8.5" stroke="#4C3043"></line>
                            <line y1="12.5" x2="18" y2="12.5" stroke="#4C3043"></line>
                        </g>
                    </svg></div>
            </div>
        </header>