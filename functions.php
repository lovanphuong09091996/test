<?php
/*==============================================================================*/
/*==============================================================================
  6. add them the ul li
 */
  
/*==============================================================================*/
/*==============================================================================
  4. Hien thi munu theme
*/
if ( ! function_exists( 'aws_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
function aws_setup() {
  
  register_nav_menus(
    array(
    'header' => __( 'Header' ),
    'menu' => __( 'Header Menu' ),
    'banner' => __( 'Banner' ), 
    'content' => __( 'Content' ),
    'footer' => __( 'Header Footer' ),
    )
);

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support(
    'html5',
    array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    )
  );

  /**
   * Add support for core custom logo.
   *
   * @link https://codex.wordpress.org/Theme_Logo
   */
  add_theme_support(
    'custom-logo',
    array(
      'height'      => 190,
      'width'       => 190,
      'flex-width'  => false,
      'flex-height' => false,
    )
  );

  // Add custom editor font sizes.
  add_theme_support(
    'editor-font-sizes',
    array(
      array(
        'name'      => __( 'Small', 'aws' ),
        'shortName' => __( 'S', 'aws' ),
        'size'      => 19.5,
        'slug'      => 'small',
      ),
      array(
        'name'      => __( 'Normal', 'aws' ),
        'shortName' => __( 'M', 'aws' ),
        'size'      => 22,
        'slug'      => 'normal',
      ),
      array(
        'name'      => __( 'Large', 'aws' ),
        'shortName' => __( 'L', 'aws' ),
        'size'      => 36.5,
        'slug'      => 'large',
      ),
      array(
        'name'      => __( 'Huge', 'aws' ),
        'shortName' => __( 'XL', 'aws' ),
        'size'      => 49.5,
        'slug'      => 'huge',
      ),
    )
  );
}
endif;
add_action( 'after_setup_theme', 'aws_setup' );
/*================================================================================
    *3. Khai bao he thong widget cua themes
    =================================================================================*/
function wpb_widgets_init() {
 
    register_sidebar( array(
      'name'          => 'banner',
      'id'            => 'banner_id',
      'description'   => 'Add to banner',
      'before_widget' => '<div class="home-hero-section__container_1rR"><div class="content_3r-">',
      'after_widget'  => '</div><div>',
      'before_title'  => '<h1 class="content__title_1aM">
                          <div class="content__desc_2kp">
                              <span class="content__desc--no-break_vBw"></span>
                          </div>
                           <a class="btn-animated content__btn_2n-" id="home_hero_shop_mattress_btn" href="/mattress">
                              <span class="btn-animated__content" btn-title="Shop The Awara organic bed"></span>
                            </a>',
      'after_title'   => '</h1>',
  ) 
    );
    register_sidebar( array(
      'name'          => 'banner-Mobile',
      'id'            => 'banner_mobile_id',
      'description'   => 'Add banner from mobile',
      'before_widget' => '<div class="chw-widget">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2 class="chw-title">',
      'after_title'   => '</h2>',
    ) 
  );
  /* Content-top*/
  register_sidebar( array(
    'name'          => 'logo_content_top',
    'id'            => 'logo_content_top',
    'description'   => 'Add logo to top content',
    'before_widget' => '<div class="content__logos-item_28r">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="content__award-title_2l_">',
    'after_title'   => '</h3>',  
  ) 
);
}
  add_action( 'widgets_init', 'wpb_widgets_init' );

    /*================================================================================
    *1. Nap nhung tap tin css vao theme
    =================================================================================*/

    function aws_styles() {
        wp_register_style( 'style1', get_template_directory_uri() . '/css/style1.min.css', 'all' );
        wp_enqueue_style( 'style1' );

        wp_register_style( 'style2', get_template_directory_uri() . '/css/style2.min.css', 'all' );
        wp_enqueue_style( 'style2' );

        wp_register_style( 'style3', get_template_directory_uri() . '/css/style3.min.css', 'all' );
        wp_enqueue_style( 'style3' );

        wp_register_style( 'style4', get_template_directory_uri() . '/css/style4.min.css', 'all' );
        wp_enqueue_style( 'style4' );

        wp_register_style( 'style5', get_template_directory_uri() . '/css/style5.min.css', 'all' );
        wp_enqueue_style( 'style5' );
      }
      add_action( 'wp_enqueue_scripts', 'aws_styles' );

      /*================================================================================
    *2. Nap nhung tap tin js vao theme
    =================================================================================*/

    class aws_Nav_Walker extends Walker_Nav_Menu {
        public function start_lvl( &$output, $depth = 0, $args = array() ) {
          $indent = str_repeat("\t", $depth);
          //$output .= "<button class=\"dropdown__button_nqJ\"></button>";
          $output .= "<button type=\"button\" data-test-id=\"home_header_bed_frames_link\" class=\"dropdown__button_nqJ\">
                        <span class=\"dropdown__button-text_3Pu\"></span>
                        <svg xmlns=\"http://www.w3.org/2000/svg\" 
                          height=\"8\" width=\"12\" viewBox=\"0 0 12 8\" 
                          fill=\"#4C3043\" class=\"dropdown__icon_20r\">
                        <path d=\"M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z\" fill=\"#C56E43\">
                        </path></svg>
                    </button>";
          //$output .= "\n$indent<li class=\"dropdown_qj6\">\n";        
          $output .= "\n$indent<ul class=\"dropdown__body_3O0\">\n";
          
        }
        public function end_lvl( &$output, $depth = 0, $args = array() ) {
          $indent = str_repeat("\t", $depth);
          //$output .= "$indent</li>\n";
          $output .= "$indent</ul>\n";
      }
      public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
          $t = '';
          $n = '';
        } else {
          $t = "\t";
          $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';
    
        $classes   = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
    
        $output .= $indent . '<li' . $id . $class_names . '>';
    
        $atts           = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target ) ? $item->target : '';
        if ( '_blank' === $item->target && empty( $item->xfn ) ) {
          $atts['rel'] = 'noopener noreferrer';
        } else {
          $atts['rel'] = $item->xfn;
        }
        $atts['href']         = ! empty( $item->url ) ? $item->url : '';
        $atts['aria-current'] = $item->current ? 'page' : '';
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
    
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
          if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
            $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
            $attributes .= ' ' . $attr . '="' . $value . '"';
          }
        }
    
        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters( 'the_title', $item->title, $item->ID );
        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );
    
        $item_output  = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $args->link_before . $title . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
      }
      public function end_el( &$output, $item, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
          $t = '';
          $n = '';
        } else {
          $t = "\t";
          $n = "\n";
        }
        $output .= "</li>{$n}";
      }
    
    } // Walker_Nav_Menu

     /** Custom logo */
     function aws_custom_logo_setup() {
      $defaults = array(
      'height'      => 25,
      'width'       => 150,
      'flex-height' => true,
      'flex-width'  => true,
      'header-text' => array( 'site-title', 'site-description' ),
      );
      add_theme_support( 'custom-logo', $defaults );
     }
     add_action( 'after_setup_theme', 'aws_custom_logo_setup' );

    /** Custom image banner */
      function aws_custom_header_setup() {
        $args = array(
          'default-image'      => get_template_directory_uri() . 'img/default-image.jpg',
          'default-text-color' => '000',
          'width'              => 1440,
          'height'             => 250,
          'flex-width'         => true,
          'flex-height'        => true,
        );  
        add_theme_support( 'custom-header',$args);  
    }
    add_action( 'after_setup_theme', 'aws_custom_header_setup' );
    /** Custom logo content */

 ?>