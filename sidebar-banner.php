
 <section id="home_hero_section" class="home-hero-section_NBR">
    <div class="home-hero-section__container_1rR">
        <div class="content_3r-">
            <h1 class="content__title_1aM">“If you like comfort and feeling truly awake when you get up in the
                morning, you’ve got to try this mattress.”</h1>
            <div class="content__desc_2kp">MADE WITH NATURAL AND ORGANIC MATERIALS,&nbsp;INCLUDING A SUPPORTIVE LAYER
                OF&nbsp;RAINFOREST
                <span class="content__desc--no-break_vBw">ALLIANCE-CERTIFIED</span>DUNLOP LATEX.
            </div>
            <a class="btn-animated content__btn_2n-" id="home_hero_shop_mattress_btn" href="/mattress">
                <span class="btn-animated__content" btn-title="Shop The Awara organic bed"></span>
            </a>
        </div>
        
    </div>
</section>
<?php //dynamic_sidebar( 'banner' ); ?> 
<section id="home_hero_mobile_section" class="hero-mobile-part_1TS">
    <div class="content_1be">
        <div class="content__desc_3sc">Made with natural and organic materials, including a supportive layer of
            RAINFOREST ALLIANCE-CERTIFIED DUNLOP LATEX.</div>
        <a class="btn-animated btn-animated--md content__btn_ycP" id="home_hero_mobile_shop_mattress_btn"
            href="/mattress"><span class="btn-animated__content" btn-title="SHOP MATTRESS"></span></a>
    </div>
</section>