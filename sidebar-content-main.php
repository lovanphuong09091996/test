<section data-test-id="home_benefits_section" class="home__benefits-section_3sz benefits_1xC">
            <div class="benefits__container_2AJ">
                <h3 class="vertical-headline vertical-headline--animated benefits__headline_3xA vertical-headline--animated-end"
                    lr="" lr-revealer-triggers="screen:-100"
                    lr-revealer-actions="addClasses:vertical-headline--animated-end">Awara Benefits</h3>
                <div class="benefits-list_2SY">
                    <div class="benefits-list__col_2_f">
                        <div class="benefit_1Wq homepage_3Uj">
                            <div class="benefit__icon-col_2lg">
                                <div class="benefit__icon-wrap_QQS"><span lr="" lr-revealer-triggers="middle:+15:vh"
                                        lr-revealer-actions="addClasses:icon--end_1eX|animateCircleIcons"
                                        class="icon_2Kb"><svg viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__circle_Zgr">
                                            <circle opacity="0.4" cx="27" cy="27" r="26" fill="none" stroke="#FED098"
                                                stroke-width="2"></circle>
                                        </svg><svg viewBox="0 0 20 21" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__inner_EG1">
                                            <path
                                                d="M10.947 0C11.7864 1.55284 12.1139 3.3311 11.8829 5.08109C11.6518 6.83108 10.874 8.46341 9.66041 9.74521C8.4468 11.027 6.85938 11.8928 5.12461 12.219C3.38984 12.5453 1.59635 12.3153 0 11.562C0.290214 13.4199 1.09536 15.1593 2.32408 16.5828C3.5528 18.0063 5.1559 19.0569 6.9515 19.6154C8.74709 20.174 10.6633 20.218 12.4826 19.7427C14.302 19.2673 15.9517 18.2915 17.2446 16.926C18.5375 15.5605 19.4217 13.8599 19.7971 12.0173C20.1724 10.1747 20.0237 8.26379 19.368 6.50137C18.7123 4.73896 17.5757 3.19561 16.0872 2.04646C14.5988 0.897297 12.818 0.188331 10.947 0V0Z">
                                            </path>
                                        </svg></span></div>
                            </div>
                            <div class="benefit__text-col_14U">
                                <h2 class="benefit__title_3gp">Deep Comfort for Anyone</h2>
                                <div class="benefit__description_2VN"><span>The Awara mattress is made with a
                                        layer of natural and organic Dunlop latex to help provide soft support
                                        with a touch of bounce, offering an ideal sleep surface for back,
                                        stomach, and side sleepers.</span></div><a
                                    class="btn-animated btn-animated--outline btn-animated--sm benefit__link_1b5"
                                    href="https://www.awarasleep.com/p/why-latex-mattress/"><span
                                        class="btn-animated__content" btn-title="LEARN MORE"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-list__col_2_f">
                        <div class="benefit_1Wq homepage_3Uj">
                            <div class="benefit__icon-col_2lg">
                                <div class="benefit__icon-wrap_QQS"><span lr="" lr-revealer-triggers="middle:+15:vh"
                                        lr-revealer-actions="addClasses:icon--end_1eX|animateCircleIcons"
                                        class="icon_2Kb"><svg viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__circle_Zgr">
                                            <circle opacity="0.4" cx="27" cy="27" r="26" fill="none" stroke="#FED098"
                                                stroke-width="2"></circle>
                                        </svg><svg viewBox="0 -2 22 22" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__inner_EG1">
                                            <path
                                                d="M19.6065 11.5305C16.5903 16.3275 11.0299 19.5 11.0299 19.5C11.0299 19.5 5.63994 16.4408 2.57239 11.9377L1.62778 10.3485C0.913461 9.00555 0.526605 7.50642 0.5 5.97821C0.5 -1.55504 9.44444 -1.57054 11 3.9917C12.5556 -1.57054 21.5 -1.57054 21.5 5.58091C21.4812 7.09787 21.1298 8.59125 20.4718 9.95123L19.6065 11.5305Z">
                                            </path>
                                        </svg></span></div>
                            </div>
                            <div class="benefit__text-col_14U">
                                <h2 class="benefit__title_3gp">A Better Night’s Sleep </h2>
                                <div class="benefit__description_2VN"><span>Sleeping on natural latex helps you
                                        create a better sleep environment that helps support your overall
                                        wellness and your best sleep. Awara uses Rainforest Alliance-Certified
                                        Dunlop latex so you can rest easy knowing you are sleeping on something
                                        better for you and the Earth.</span></div><a
                                    class="btn-animated btn-animated--outline btn-animated--sm benefit__link_1b5"
                                    href="https://www.awarasleep.com/p/why-latex-mattress/"><span
                                        class="btn-animated__content" btn-title="LEARN MORE"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-list__col_2_f">
                        <div class="benefit_1Wq homepage_3Uj">
                            <div class="benefit__icon-col_2lg">
                                <div class="benefit__icon-wrap_QQS"><span lr="" lr-revealer-triggers="middle:+15:vh"
                                        lr-revealer-actions="addClasses:icon--end_1eX|animateCircleIcons"
                                        class="icon_2Kb"><svg viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__circle_Zgr">
                                            <circle opacity="0.4" cx="27" cy="27" r="26" fill="none" stroke="#FED098"
                                                stroke-width="2"></circle>
                                        </svg><svg viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg"
                                            class="icon__inner_EG1">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M21.5 10.75C21.5 16.6871 16.6871 21.5 10.75 21.5C4.81294 21.5 0 16.6871 0 10.75C0 4.81294 4.81294 0 10.75 0C16.6871 0 21.5 4.81294 21.5 10.75ZM10.7501 19.7083C8.97824 19.7084 7.24619 19.183 5.77296 18.1987C4.29972 17.2143 3.15148 15.8152 2.47342 14.1782C1.79537 12.5413 1.61796 10.74 1.96364 9.00225C2.30932 7.26447 3.16256 5.66823 4.41545 4.41538C4.48303 4.35011 4.57355 4.31399 4.6675 4.31481C4.76145 4.31562 4.85133 4.35331 4.91777 4.41975C4.98421 4.48619 5.02189 4.57606 5.02271 4.67001C5.02352 4.76397 4.98741 4.85448 4.92213 4.92207C3.58476 6.25948 2.74807 8.01619 2.55247 9.89741C2.35688 11.7786 2.81427 13.6699 3.8479 15.2538C4.88154 16.8378 6.42851 18.018 8.22924 18.5965C10.03 19.1749 11.9749 19.1164 13.7376 18.4308C15.5003 17.7452 16.9736 16.4741 17.9102 14.831C18.8467 13.1878 19.1897 11.2725 18.8814 9.40639C18.5731 7.54032 17.6324 5.83707 16.2171 4.58243C14.8018 3.32779 12.998 2.59812 11.1084 2.51586V5.87667C11.1084 5.9717 11.0706 6.06285 11.0034 6.13005C10.9362 6.19725 10.8451 6.235 10.7501 6.235C10.655 6.235 10.5639 6.19725 10.4967 6.13005C10.4295 6.06285 10.3917 5.9717 10.3917 5.87667V2.15C10.3917 2.05496 10.4295 1.96382 10.4967 1.89662C10.5639 1.82942 10.655 1.79167 10.7501 1.79167C13.126 1.79167 15.4046 2.73549 17.0846 4.4155C18.7646 6.09552 19.7084 8.3741 19.7084 10.75C19.7084 13.1259 18.7646 15.4045 17.0846 17.0845C15.4046 18.7645 13.126 19.7083 10.7501 19.7083ZM9.80935 12.049L6.19663 7.06167C6.14209 6.99228 6.11487 6.9053 6.12013 6.81718C6.12539 6.72907 6.16276 6.64595 6.22517 6.58353C6.28759 6.52112 6.37072 6.48375 6.45883 6.47849C6.54694 6.47323 6.63392 6.50044 6.70332 6.55499L11.6906 10.1677C11.97 10.3875 12.1506 10.7092 12.1927 11.0622C12.2348 11.4151 12.135 11.7703 11.9153 12.0497C11.6955 12.329 11.3738 12.5096 11.0208 12.5518C10.6679 12.5939 10.3127 12.4941 10.0333 12.2743C9.95036 12.2079 9.87522 12.1323 9.80935 12.049Z">
                                            </path>
                                        </svg></span></div>
                            </div>
                            <div class="benefit__text-col_14U">
                                <h2 class="benefit__title_3gp">Designed for Undisturbed Rest</h2>
                                <div class="benefit__description_2VN"><span>As a hybrid, Awara is designed to
                                        provide the best of both worlds. Enjoy the firm support of premium,
                                        individually-wrapped pocket coils combined with the cradling contour of
                                        the natural, organic latex layer.</span></div><a
                                    class="btn-animated btn-animated--outline btn-animated--sm benefit__link_1b5"
                                    href="https://www.awarasleep.com/p/why-latex-mattress/"><span
                                        class="btn-animated__content" btn-title="LEARN MORE"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-test-id="home_natural_organic_latex_section" class="home__natural-organic-latex-section_1a3">
            <div class="proof-grid_UC0">
                <div class="proof-grid__row_2b_">
                    <div class="proof-grid__img-col_1cv">
                        <div class="proof-grid__img-wrap_1vM">
                            <div class="animation__fade-in-up proof-grid__img_2hF animation__fade-in-up--end" lr=""
                                lr-loader-triggers="screen:+300"
                                lr-loader-actions="imageLg:https://media.awarasleep.com/awara/home-organic-and-voc-free.jpg?width=1000&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/home-organic-and-voc-free.jpg?width=768&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/home-organic-and-voc-free.jpg?width=768&amp;auto=webp"
                                lr-revealer-triggers="middle:+100"
                                lr-revealer-actions="setBg|addClasses:animation__fade-in-up--end" role="img"
                                aria-label="Organic and Natural Latex Mattress"
                                style="background-image: url(&quot;https://media.awarasleep.com/awara/home-organic-and-voc-free.jpg?width=1000&amp;auto=webp&quot;);">
                            </div>
                        </div>
                    </div>
                    <div class="proof-grid__content-col_3hD">
                        <div class="proof-grid__content-wrap_2-W">
                            <div class="content_1-M">
                                <h3 class="vertical-headline vertical-headline--animated content__headline_3J- vertical-headline--animated-end"
                                    lr="" lr-revealer-triggers="screen:-100"
                                    lr-revealer-actions="addClasses:vertical-headline--animated-end">Natural &amp;
                                    Organic Latex</h3>
                                <h2 class="content__title_2X4">Organic, and <span class="no-wrap">VOC-Free</span>
                                    Materials</h2>
                                <div class="content__desc_rw_">Awara’s natural and organic latex offers <span
                                        class="bold_2BH">body-contouring</span> and <span
                                        class="bold_2BH">pressure-point support</span>. When paired with a super-soft
                                    quilted <span class="bold_2BH">Euro top layer</span> made with
                                    <span class="bold_2BH">organic cotton</span>, the result provides an <span
                                        class="bold_2BH">ideal sleep experience</span>. We’ve chosen to go as
                                    <span class="bold_2BH">natural and chemical-free</span> as possible (down to our
                                    water-based adhesives) which means a mattress that’s better for you, the
                                    environment, and, of course, your sleep.</div><a
                                    class="btn-animated btn-animated--sm_127 btn-animated--outline content__button_3QY"
                                    href="https://www.awarasleep.com/p/about/"><span btn-title="ABOUT US"
                                        class="btn-animated__content"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div data-test-id="home_giant_cta_section" class="giant-cta-container_3rs">
            <a class="giant-cta_wsj fade-in-border__start_3hy fade-in-border__end_1Rt"
                href="https://www.awarasleep.com/p/finance-affirm/" lr="" lr-revealer-triggers="screen:-15:vh"
                lr-revealer-actions="addClasses:fade-in-border__end_1Rt|animateGiantCtaBorder"><span
                    class="cta-border_1mD"><svg viewBox="0 0 486 120" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="cta-border__left-rectangle_2xO">
                        <rect x="1" y="1" width="484" height="118" rx="59" fill="none" stroke-width="2"
                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                    </svg><svg viewBox="0 0 486 120" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="cta-border__right-rectangle_2wi">
                        <rect x="1" y="1" width="484" height="118" rx="59" fill="none" stroke-width="2"
                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                    </svg></span><span class="cta-content_3Id"><span
                        class="cta-content__circle-icon-wrap_3RQ fade-in-right__start_20J fade-in-right__end_3Z6" lr=""
                        lr-revealer-triggers="screen:-15:vh"
                        lr-revealer-actions="addClasses:fade-in-right__end_3Z6"><svg viewBox="0 0 90 90" fill="none"
                            xmlns="http://www.w3.org/2000/svg" class="cta-content__circle-icon_29N">
                            <circle opacity="0.4" cx="45" cy="45" r="44" stroke-width="2"></circle>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M59 62V32.7414C58.9827 30.2902 57.1188 28.2878 54.7371 28.0116C53.1492 27.9081 51.4579 28.495 50.4569 29.7896L25 62H31.2822C33.7848 62 35.7695 60.6881 37.2883 58.7548L51.3888 40.9062V62H59Z">
                            </path>
                        </svg></span><span class="cta-content__headings_3Ad"><span
                            class="cta-content__heading_kcP"><span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:0:fade-in-up__end_2Be">As</span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:130:fade-in-up__end_2Be">low</span>
                                <span class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:260:fade-in-up__end_2Be">as</span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:390:fade-in-up__end_2Be">0%</span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:520:fade-in-up__end_2Be">APR</span>
                                <span class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:650:fade-in-up__end_2Be">with&nbsp;Affirm</span>
                            </span>
                        </span><span class="cta-content__sub-heading_2Qq"><span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:800:fade-in-up__end_2Be">Learn</span><span
                                    class="word_NLD fade-in-up__start_1b2 fade-in-up__end_2Be" lr=""
                                    lr-revealer-triggers="screen:-15vh"
                                    lr-revealer-actions="addClassesDelay:930:fade-in-up__end_2Be">More</span></span>
                        </span>
                    </span><span class="cta-content__arrow-icon-wrap_kIb fade-in-left__start_1pe fade-in-left__end_bvH"
                        lr="" lr-revealer-triggers="screen:-15:vh"
                        lr-revealer-actions="addClassesDelay:600:fade-in-left__end_bvH"><svg viewBox="0 0 21 16"
                            xmlns="http://www.w3.org/2000/svg" class="cta-content__arrow-icon_1BE">
                            <path
                                d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z">
                            </path>
                        </svg></span></span>
            </a>
        </div>
        <section data-test-id="home_luxurious_natural_latex_section" class="">
            <div class="proof-grid_UC0 image-right-bottom_1Hr">
                <div class="proof-grid__row_2b_">
                    <div class="proof-grid__img-col_1cv">
                        <div class="proof-grid__img-wrap_1vM">
                            <div class="animation__fade-in-up proof-grid__img_2hF animation__fade-in-up--end" lr=""
                                lr-loader-triggers="screen:+300"
                                lr-loader-actions="imageLg:https://media.awarasleep.com/awara/luxurious-natural-latex-mattress-desktop-v1.jpg?width=1000&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/luxurious-natural-latex-mattress-tablet-v1.jpg?width=768&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/luxurious-natural-latex-mattress-mobile-v1.jpg?width=768&amp;auto=webp"
                                lr-revealer-triggers="middle:+100"
                                lr-revealer-actions="setBg|addClasses:animation__fade-in-up--end" role="img"
                                aria-label="Luxurious And Natural Latex Mattress"
                                style="background-image: url(&quot;https://media.awarasleep.com/awara/luxurious-natural-latex-mattress-desktop-v1.jpg?width=1000&amp;auto=webp&quot;);">
                            </div>
                        </div>
                    </div>
                    <div class="proof-grid__content-col_3hD">
                        <div class="proof-grid__content-wrap_2-W">
                            <div class="content_2GA">
                                <h3 class="vertical-headline vertical-headline--animated content__headline_35d vertical-headline--animated-end"
                                    lr="" lr-revealer-triggers="screen:-100"
                                    lr-revealer-actions="addClasses:vertical-headline--animated-end">Luxurious And
                                    Natural Latex</h3>
                                <h2 class="content__title_3db">The Very Best in Comfort and Support</h2>
                                <div class="content__desc_S2_">We hand-selected and tested each of our natural and
                                    organic materials to find what we are confident is the best of the best. That
                                    includes Rainforest Alliance-Certified Dunlop latex, known for its supple firmness,
                                    organic cotton to help provide breathability and comfort, and Certified 100% Organic
                                    Wool fiber from New Zealand to help with temperature control. The result is a
                                    mattress that helps support your overall well-being
                                    and provides ideal support for any sleeping position.
                                </div><a
                                    class="btn-animated btn-animated--sm_127 btn-animated--outline content__button_2fh"
                                    href="https://www.awarasleep.com/p/about/"><span btn-title="ABOUT US"
                                        class="btn-animated__content"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div data-test-id="home_quote_section_1" id="home_quote_section_1" class="home__quote-section_sXn quote_yUv">
            <div class="quote__container_2_y">
                <div class="quote__stars_3Ad"><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20"
                        viewBox="0 0 20 19" class="quote__star_k8s">
                        <path
                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                        </path>
                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                        class="quote__star_k8s">
                        <path
                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                        </path>
                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                        class="quote__star_k8s">
                        <path
                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                        </path>
                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                        class="quote__star_k8s">
                        <path
                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                        </path>
                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                        class="quote__star_k8s">
                        <path
                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                        </path>
                    </svg></div>
                <div class="quote__body_2uk">THE AWARA MATTRESS MADE A WORLD OF DIFFERENCE IN MY SLEEP. WOW AM I WELL
                    RESTED! IT TRULY IS THE PERFECT COMBINATION <span class="no-wrap">OF SOFT</span> AND SUPPORTIVE AND
                    BOUNCY. KNEW AFTER THE FIRST WEEK THAT I WON’T BE SLEEPING
                    ON ANYTHING ELSE.
                </div>
                <h6 class="quote__author__jO">Elise, San Francisco CA</h6>
                <div class="quote__buttons_3YH"><a class="btn-animated btn-animated--md quote__button_2nH"
                        href="/mattress"><span class="btn-animated__content" btn-title="SHOP MATTRESS"></span></a><a
                        class="btn-animated btn-animated--outline btn-animated--sm quote__button_2nH"
                        href="/reviews"><span class="btn-animated__content" btn-title="SEE REVIEWS"></span></a>
                </div>
            </div>
        </div>