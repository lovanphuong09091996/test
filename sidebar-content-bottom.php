<section data-test-id="home_hybrid-design_section" class="home__hybrid-design-section_2wK">
    <div class="proof-grid_UC0 image-left_620_3Pc">
        <div class="proof-grid__row_2b_">
            <div class="proof-grid__img-col_1cv">
                <div class="proof-grid__img-wrap_1vM">
                    <div class="animation__fade-in-up proof-grid__img_2hF animation__fade-in-up--end" lr=""
                        lr-loader-triggers="screen:+300"
                        lr-loader-actions="imageLg:https://media.awarasleep.com/awara/mattress-with-hybrid-design-desktop-v2.jpg?width=1000&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/mattress-with-hybrid-design-desktop-v2.jpg?width=768&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/mattress-with-hybrid-design-desktop-v2.jpg?width=768&amp;auto=webp"
                        lr-revealer-triggers="middle:+100"
                        lr-revealer-actions="setBg|addClasses:animation__fade-in-up--end" role="img"
                        aria-label="Mattress with Hybrid Design"
                        style="background-image: url(&quot;https://media.awarasleep.com/awara/mattress-with-hybrid-design-desktop-v2.jpg?width=1000&amp;auto=webp&quot;);">
                    </div>
                </div>
            </div>
            <div class="proof-grid__content-col_3hD">
                <div class="proof-grid__content-wrap_2-W">
                    <div class="content_2Mw">
                        <h3 class="vertical-headline vertical-headline--animated content__headline_387 vertical-headline--animated-end"
                            lr="" lr-revealer-triggers="screen:-100"
                            lr-revealer-actions="addClasses:vertical-headline--animated-end">Hybrid Design
                        </h3>
                        <h2 class="content__title_2bH">Premium Individually-Wrapped Pocket Coil System
                        </h2>
                        <div class="content__desc_2pn"><span>The Awara Mattress features a 9-inch
                                support layer made up of premium, individually-wrapped pocket coils to
                                help provide full body support and proper spinal alignment while you
                                sleep. Reinforced edging helps the coils evenly distribute weight and
                                reduce motion transfer for undisturbed rest. Combined with the
                                contouring latex, the premium coil system also gives Awara that touch of
                                bounce that our customers love.</span></div><a
                            class="btn-animated  btn-animated--md content__button_3In" href="/mattress"><span
                                btn-title="SHOP MATTRESS" class="btn-animated__content"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home_our_guarantee_section" class="guarantee-section_1ZV">
    <div class="guarantee-section__headline-wrap_2yY">
        <h3 class="vertical-headline vertical-headline--animated vertical-headline--centered guarantee-section__headline_1ZD vertical-headline--animated-end"
            lr="" lr-revealer-triggers="screen:-100" lr-revealer-actions="addClasses:vertical-headline--animated-end">
            Our Guarantee</h3>
    </div>
    <div class="guarantee-section__row_3OM">
        <div class="guarantee-section__col_1Q3">
            <div class="guarantee-section__first-block_3PM">
                <div class="guarantee-block_3FE">
                    <div class="guarantee-block__boxes-wrap_2cp">
                        <div class="guarantee-block__boxes_1s-">
                            <div class="guarantee-block__box_1tS"><a class="guarantee-box_3u3 big-transparent_USp"
                                    href="https://www.awarasleep.com/p/warranty/"><span
                                        class="text-line_28n">FOREVER</span><span
                                        class="text-line_28n">WARRANTY</span></a></div>
                        </div>
                    </div>
                    <div class="guarantee-block__content_ve1">
                        <div class="content__desc_3SP">We believe in a model that’s sustainable for the planet,
                            and your lifestyle. Awara mattresses are built to last a lifetime, and are
                            guaranteed with our industry-leading Forever Warranty™. We guarantee that, in the
                            unlikely chance that you experience a manufacturing defect, we will repair or
                            replace your Awara mattress for free for as long as you own and use it. Enjoy the
                            best sleep of your life, for the rest of your life.
                            For more information on our warranty, click here.</div>
                        <a class="btn-animated btn-animated--outline btn-animated--sm_127"
                            href="https://www.awarasleep.com/p/warranty/"
                            id="home_our_guarantee_shop_mattress_btn"><span class="btn-animated__content"
                                btn-title="LEARN MORE"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="guarantee-section__col_1Q3">
            <div class="guarantee-section__second-block_2kZ">
                <div class="guarantee-block_1fV">
                    <div class="guarantee-block__boxes-wrap_iW3">
                        <div class="guarantee-block__boxes_3fF">
                            <div class="guarantee-block__box_1rH"><a class="guarantee-box_3u3 big-transparent_USp"
                                    href="https://www.awarasleep.com/p/trial/"><span
                                        class="text-line_28n text-line--days_jY4">365</span><span
                                        class="text-line_28n">NIGHT TRIAL</span></a></div>
                            <div class="guarantee-block__box_1rH"><a class="guarantee-box_3u3 big-transparent_USp"
                                    href="/faq#shipping"><span class="text-line_28n">FREE</span><span
                                        class="text-line_28n">SHIPPING
                                        &amp;</span><span class="text-line_28n">RETURNS</span></a></div>
                        </div>
                    </div>
                    <div class="guarantee-block__content_chI">
                        <div class="content__desc_nNJ">With Awara you get a whole year to try it out in the
                            comfort of your home. Awara is shipped directly to your doorstep and comes with
                            durable handles so you can move the mattress yourself - or select our optional
                            White Glove Service to have the mattress set up in the room of your choice. With our
                            365-night trial that comes with free shipping and returns, Awara gives you the time
                            you need to be confident in your sleep.
                        </div><a class="btn-animated btn-animated--outline btn-animated--sm_127"
                            href="https://www.awarasleep.com/p/trial/" id="home_our_guarantee_shop_mattress_btn"><span
                                class="btn-animated__content" btn-title="LEARN MORE"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section data-test-id="home_sustainability_section" class="home__sustain-section_16v ">
    <div class="container container--large-screen-max_none container--padding_0">
        <div class="sustain__row_2UW">
            <div class="sustain__content-col_2cg">
                <div class="content_SeO">
                    <h3 class="vertical-headline vertical-headline--animated content__headline_19- vertical-headline--animated-end"
                        lr="" lr-revealer-triggers="screen:-100"
                        lr-revealer-actions="addClasses:vertical-headline--animated-end">Sustainability
                    </h3>
                    <h2 class="content__title_giY">10 Trees. A Family’s Future. <br
                            class="content__title-break_k3G">Your powerful choice.</h2>
                    <p class="content__desc_3o5">For every Awara purchase, we plant 10 fruit trees to nourish
                        farming communities and their land in areas like Kenya, Uganda &amp; Guinea. This means
                        food security for generations of farmers families and land restoration
                        where it’s urgently needed.</p>
                    <p class="content__desc_3o5">It starts with your decision to buy consciously. With 1
                        powerful choice.</p>
                    <a class="btn-animated btn-animated--sm btn-animated--outline content__button_sLm"
                        href="https://www.awarasleep.com/p/our-sustainability-pledge/"><span
                            class="btn-animated__content" btn-title="LEARN MORE"></span></a>
                </div>
            </div>
            <div class="sustain__img-col_16Z">
                <div class="animation__fade-in-up sustain-image_3b5 animation__fade-in-up--end" lr=""
                    lr-loader-triggers="screen:+500"
                    lr-loader-actions="imageLg:https://media.awarasleep.com/awara/sustainability-planting-trees-protect-world-desktop-v2.jpg?width=1000&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/sustainability-planting-trees-protect-world-desktop-v2.jpg?width=768&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/sustainability-planting-trees-protect-world-desktop-v2.jpg?width=768&amp;auto=webp"
                    lr-revealer-triggers="screen:-10:vh"
                    lr-revealer-actions="setBg|addClasses:animation__fade-in-up--end" role="img"
                    aria-label="Sustainability - Planting Trees to Protect Our World"
                    style="background-image: url(&quot;https://media.awarasleep.com/awara/sustainability-planting-trees-protect-world-desktop-v2.jpg?width=1000&amp;auto=webp&quot;);">
                </div>
            </div>
        </div>
    </div>
</section>
<div data-test-id="home_quote_section_2" id="home_quote_section_2" class="home__quote-section_sXn quote_yUv">
    <div class="quote__container_2_y">
        <div class="quote__stars_3Ad"><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                class="quote__star_k8s">
                <path
                    d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                </path>
            </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                class="quote__star_k8s">
                <path
                    d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                </path>
            </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                class="quote__star_k8s">
                <path
                    d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                </path>
            </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                class="quote__star_k8s">
                <path
                    d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                </path>
            </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                class="quote__star_k8s">
                <path
                    d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                </path>
            </svg></div>
        <div class="quote__body_2uk">I LOVED THAT AWARA GIVES YOU A <span class="bold_1cj">365 NIGHT
                TRIAL</span> FOR THE MATTRESS BUT WE DIDN’T NEED IT. THE HYBRID IS THE PERFECT COMBINATION OF
            SOFT AND FIRM. THERE IS NO WAY WE WOULD SEND THIS MATTRESS BACK, DEFINITELY A FOREVER MATTRESS!
        </div>
        <h6 class="quote__author__jO">Mark, Athens OH</h6>
        <div class="quote__buttons_3YH"><a class="btn-animated btn-animated--md quote__button_2nH"
                href="/mattress"><span class="btn-animated__content" btn-title="SHOP MATTRESS"></span></a><a
                class="btn-animated btn-animated--outline btn-animated--sm quote__button_2nH" href="/reviews"><span
                    class="btn-animated__content" btn-title="SEE REVIEWS"></span></a>
        </div>
    </div>
</div>
<div class="home__trial-section_1Qb">
    <div data-test-id="home_trial_section" class="container--max-width_1440 trial-section_ZuC">
        <div class="trial-section__left_37B">
            <div class="trial-section__headline_2vT">
                <h5 class="headline-animated_1OY headline-animated--end_1yB" headline-text="Trial &amp; Warranty" lr=""
                    lr-revealer-triggers="screen:-15:vh" lr-revealer-actions="addClasses:headline-animated--end_1yB">
                </h5>
            </div>
            <div class="trial-section__bg_2HK">
                <h2 class="trial-section__h_1zw">What’s Better Than Forever?</h2>
                <p class="trial-section__p_AGo"><span class="above-mobile_1gs">MADE WITH A LAYER OF
                        RAINFOREST ALLIANCE-CERTIFIED DUNLOP LATEX, INSPIRED BY NATURE AND MADE WITH
                        NATURAL AND ORGANIC MATERIALS TO BRING YOU YOUR BEST SLEEP.</span><span
                        class="mobile-only_2et">MADE FROM NATURAL &amp; ORGANIC DUNLOP LATEX, BORN FROM
                        NATURE AND LOVINGLY CRAFTED TO BRING YOU YOUR BEST SLEEP.</span></p><a
                    class="btn-animated btn-animated--outline btn-animated--sm trial-section__cta_2fq"
                    href="/mattress"><span btn-title="LEARN MORE" class="btn-animated__content"></span></a>
                <div class="guaranties-grid_12w">
                    <div class="guaranties-grid__cell_3Uh">
                        <a class="guarantee-box-animated guaranties-grid__box_3FH guarantee-box-animated--end"
                            href="https://www.awarasleep.com/p/trial/" lr="" lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="addClassesDelay:500:guarantee-box-animated--end|animateBoxBorder:500"><span
                                class="guarantee-box-animated__box-wrap"><span
                                    class="guarantee-box-animated__border"><svg viewBox="-1 -1 52 52"
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__left-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                                    </svg><svg viewBox="-1 -1 52 52" xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__right-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="300"></rect>
                                    </svg></span><span class="guarantee-box-animated__content"><span
                                        class="days-animated"><span class="days-animated__number-wrap"><span
                                                class="days-animated__number days-animated__number--first">3<br>2<br>1<br>0</span></span><span
                                            class="days-animated__number-wrap"><span
                                                class="days-animated__number days-animated__number--second">6<br>5<br>4<br>3<br>2<br>1<br>0</span></span><span
                                            class="days-animated__number-wrap"><span
                                                class="days-animated__number days-animated__number--third">5<br>4<br>3<br>2<br>1<br>0</span></span>
                                    </span><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--fixed">NIGHT
                                        TRIAL</span></span>
                            </span>
                        </a>
                        <div class="guaranties-grid__text_1Fk">Enjoy a full year risk-free trial before deciding
                            Awara is right.</div>
                    </div>
                    <div class="guaranties-grid__cell_3Uh"><a
                            class="guarantee-box-animated guaranties-grid__box_3FH guarantee-box-animated--end" lr=""
                            lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="addClassesDelay:500:guarantee-box-animated--end|animateBoxBorder:500"
                            href="/faq#shipping"><span class="guarantee-box-animated__box-wrap"><span
                                    class="guarantee-box-animated__border"><svg viewBox="-1 -1 52 52"
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__left-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                                    </svg><svg viewBox="-1 -1 52 52" xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__right-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="300"></rect>
                                    </svg></span><span class="guarantee-box-animated__content"><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--first">FREE</span><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--second">SHIPPING
                                        &amp;</span><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--third">RETURNS</span></span></span></a>
                        <div class="guaranties-grid__text_1Fk">If you’re not 100% satisfied, get a full refund.
                        </div>
                    </div>
                    <div class="guaranties-grid__cell_3Uh">
                        <a class="guarantee-box-animated guaranties-grid__box_3FH guarantee-box-animated--end"
                            href="https://www.awarasleep.com/p/finance-affirm/" lr=""
                            lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="addClassesDelay:500:guarantee-box-animated--end|animateBoxBorder:500"><span
                                class="guarantee-box-animated__box-wrap"><span
                                    class="guarantee-box-animated__border"><svg viewBox="-1 -1 52 52"
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__left-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                                    </svg><svg viewBox="-1 -1 52 52" xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__right-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="300"></rect>
                                    </svg></span><span class="guarantee-box-animated__content"><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--second">AS
                                        LOW AS</span><span
                                        class="finance-affirm-box__percent-text guarantee-box-animated__line guarantee-box-animated__line--third">0%
                                        APR</span></span>
                            </span>
                        </a>
                        <div class="guaranties-grid__text_1Fk">Choose Affirm at Checkout.</div>
                    </div>
                    <div class="guaranties-grid__cell_3Uh">
                        <a class="guarantee-box-animated guaranties-grid__box_3FH guarantee-box-animated--end"
                            href="https://www.awarasleep.com/p/warranty/" lr="" lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="addClassesDelay:500:guarantee-box-animated--end|animateBoxBorder:500"><span
                                class="guarantee-box-animated__box-wrap"><span
                                    class="guarantee-box-animated__border"><svg viewBox="-1 -1 52 52"
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__left-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="0"></rect>
                                    </svg><svg viewBox="-1 -1 52 52" xmlns="http://www.w3.org/2000/svg"
                                        class="guarantee-box-animated__right-rectangle">
                                        <rect x="0" y="0" width="50" height="50" fill="none" stroke-width="2"
                                            stroke-linecap="square" stroke-dashoffset="300"></rect>
                                    </svg></span><span class="guarantee-box-animated__content"><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--second">FOREVER</span><span
                                        class="guarantee-box-animated__text guarantee-box-animated__line guarantee-box-animated__line--third">WARRANTY</span></span>
                            </span>
                        </a>
                        <div class="guaranties-grid__text_1Fk">Maintain a premium experience over a lifetime.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="animation__fade-in-up trial-image_1dk trial-image--revealed_19h animation__fade-in-up--end" lr=""
            lr-loader-triggers="screen:+300"
            lr-loader-actions="imageC:1025+:https://media.awarasleep.com/awara/gols-certified-organic-dunlop-latex-lg-v3.jpg?width=740&amp;auto=webp|imageC:768-1024:https://media.awarasleep.com/awara/gols-certified-organic-dunlop-latex-md-v3.jpg?width=1024&amp;auto=webp|imageC:0-767:https://media.awarasleep.com/awara/gols-certified-organic-dunlop-latex-xs-v3.jpg?width=768&amp;auto=webp"
            lr-revealer-triggers="screen:-10:vh"
            lr-revealer-actions="addClasses:trial-image--revealed_19h:animation__fade-in-up--end" role="img"
            aria-label="GOLS-Certified Organic Dunlop Latex"></div>
    </div>
</div>
<section data-test-id="home_prefooter_section" class="pre-footer-section_1he">
    <div class="container--max-width_1440">
        <div class="pre-footer-section__row_3gh">
            <div class="pre-footer-section__image-col_3d8">
                <figure class="pre-footer__image-wrapper_1Zg">
                    <div class="pre-footer__image--desktop-mobile_10s"><img
                            class="animation__fade-in-up animation__fade-in-up--end" lr=""
                            lr-loader-triggers="screen:+500"
                            lr-loader-actions="image:https://media.awarasleep.com/awara/awara-mattress-desktop-v2.jpg?width=728&amp;auto=webp"
                            lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="setSrc|addClasses:animation__fade-in-up--end" alt="Awara Mattress"
                            src="https://media.awarasleep.com/awara/awara-mattress-desktop-v2.jpg?width=728&amp;auto=webp">
                    </div>
                    <div class="pre-footer__image--tablet_1mW"><img class="animation__fade-in-up" lr=""
                            lr-loader-triggers="screen:+500"
                            lr-loader-actions="image:https://media.awarasleep.com/awara/awara-mattress-tablet-v1.jpg?width=768&amp;auto=webp"
                            lr-revealer-triggers="screen:-10:vh"
                            lr-revealer-actions="setSrc|addClasses:animation__fade-in-up--end" alt="Awara Mattress">
                    </div>
                </figure>
            </div>
            <div class="pre-footer-section__latex-section-col_29j">
                <article class="pre-footer__latex-section_2uK">
                    <div class="pre-footer__latex-info-container_1Eq">
                        <h2 class="pre-footer__latex-title_E1r">Awara Hybrid Mattress</h2>
                        <a href="/reviews">
                            <div class="review-link_3Yw desert_2HG"><span class="review-link__stars_3vZ"><svg
                                        xmlns="http://www.w3.org/2000/svg" height="19" width="20" viewBox="0 0 20 19"
                                        class="review-link__star_2G7">
                                        <path
                                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                                        </path>
                                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20"
                                        viewBox="0 0 20 19" class="review-link__star_2G7">
                                        <path
                                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                                        </path>
                                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20"
                                        viewBox="0 0 20 19" class="review-link__star_2G7">
                                        <path
                                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                                        </path>
                                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20"
                                        viewBox="0 0 20 19" class="review-link__star_2G7">
                                        <path
                                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                                        </path>
                                    </svg><svg xmlns="http://www.w3.org/2000/svg" height="19" width="20"
                                        viewBox="0 0 20 19" class="review-link__star_2G7">
                                        <path
                                            d="M10 0.00109863L12.2451 6.91093H19.5106L13.6327 11.1814L15.8779 18.0913L10 13.8208L4.12215 18.0913L6.36729 11.1814L0.489435 6.91093H7.75486L10 0.00109863Z">
                                        </path>
                                    </svg></span><span class="review-link__text_16G">READ REVIEWS</span>
                            </div>
                        </a>
                        <p class="pre-footer__latex-paragraph_2IF">Awara is a hybrid mattress made with
                            Rainforest Alliance-Certified Dunlop latex featuring natural support that contours
                            perfectly to your body for your best night’s sleep. </p><a
                            class="btn-animated pre-footer__button_2JV" href="/mattress"><span
                                class="btn-animated__content" btn-title="SHOP MATTRESS"></span></a>
                    </div>
                </article>
                <div class="pre-footer__affirm-section_2xm pre-footer__affirm-section--desktop_29S">
                    <div class="pre-footer__affirm-container_1oA"><a class="pre-footer__affirm-link_3SZ"
                            href="https://www.awarasleep.com/p/finance-affirm/"><svg xmlns="http://www.w3.org/2000/svg"
                                height="40px" width="40px" viewBox="0 0 40 40" fill="none" class="icon_3Uw">
                                <g>
                                    <path
                                        d="M20 40C8.93334 40 0 31.0667 0 20C0 8.93334 8.93334 0 20 0C31.0667 0 40 8.93334 40 20C40 31.0667 30.9333 40 20 40ZM20 1.73333C10 1.73333 1.86667 9.86666 1.86667 19.8667C1.86667 29.8667 10 38 20 38C30 38 38.1333 29.8667 38.1333 19.8667C38.1333 9.86666 30 1.73333 20 1.73333Z"
                                        fill="#4C3043"></path>
                                    <path
                                        d="M26.1326 13.3336C26.1326 12.0003 25.0659 10.9336 23.8659 10.8003C22.9326 10.8003 22.1325 11.067 21.5992 11.7336L14.5326 20.667L11.3326 24.8003L10.3992 26.0003H13.5992C14.3992 26.0003 15.1992 25.6003 15.5992 25.067L15.7326 24.9336L22.5326 16.267V26.0003H26.1326V13.3336Z"
                                        fill="#4C3043"></path>
                                </g>
                            </svg>
                            <div>
                                <h3>As low as 0% APR with Affirm</h3><span
                                    class="pre-footer__affirm-learn-more_2C7">Learn More</span>
                            </div>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="pre-footer__affirm-section_2xm pre-footer__affirm-section--tablet-mobile_1l9">
        <div class="pre-footer__affirm-container_1oA"><a class="pre-footer__affirm-link_3SZ"
                href="https://www.awarasleep.com/p/finance-affirm/"><svg xmlns="http://www.w3.org/2000/svg"
                    height="40px" width="40px" viewBox="0 0 40 40" fill="none" class="icon_3Uw">
                    <g>
                        <path
                            d="M20 40C8.93334 40 0 31.0667 0 20C0 8.93334 8.93334 0 20 0C31.0667 0 40 8.93334 40 20C40 31.0667 30.9333 40 20 40ZM20 1.73333C10 1.73333 1.86667 9.86666 1.86667 19.8667C1.86667 29.8667 10 38 20 38C30 38 38.1333 29.8667 38.1333 19.8667C38.1333 9.86666 30 1.73333 20 1.73333Z"
                            fill="#4C3043"></path>
                        <path
                            d="M26.1326 13.3336C26.1326 12.0003 25.0659 10.9336 23.8659 10.8003C22.9326 10.8003 22.1325 11.067 21.5992 11.7336L14.5326 20.667L11.3326 24.8003L10.3992 26.0003H13.5992C14.3992 26.0003 15.1992 25.6003 15.5992 25.067L15.7326 24.9336L22.5326 16.267V26.0003H26.1326V13.3336Z"
                            fill="#4C3043"></path>
                    </g>
                </svg>
                <div>
                    <h3>As low as 0% APR with Affirm</h3><span class="pre-footer__affirm-learn-more_2C7">Learn
                        More</span>
                </div>
            </a></div>
    </div>
</section>